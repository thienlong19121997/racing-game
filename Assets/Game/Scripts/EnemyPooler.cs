﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPooler : MonoBehaviour {

    public static EnemyPooler instance;

    public GameObject prefab;
    private List<GameObject> listObjects = new List<GameObject>();
    private List<EnemyCar> enemyCars = new List<EnemyCar>();
    private const int count = 10;

    private void Awake()
    {
        instance = this;
    }


    void Start () {
		for(int i = 0; i < count; i++)
        {
            GenerateObject();
        }
	}
	
    private void GenerateObject()
    {
        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        objectClone.SetActive(false);
        listObjects.Add(objectClone);
        enemyCars.Add(objectClone.GetComponent<EnemyCar>());
    }

    public GameObject GetObject()
    {
        for(int i = 0; i < listObjects.Count; i++)
        {
            if(listObjects[i].activeInHierarchy == false)
            {
                return listObjects[i];
            }
        }

        GameObject objectClone = Instantiate(prefab, transform) as GameObject;
        objectClone.SetActive(false);
        listObjects.Add(objectClone);
        enemyCars.Add(objectClone.GetComponent<EnemyCar>());
        return objectClone;
    }

    private void Update()
    {
        if (Car.instance.isComplete || Car.instance.isStartGame == false)
            return;

        for(int i = 0; i < listObjects.Count; i++)
        {
            if (listObjects[i].activeSelf)
            {
                enemyCars[i].UpdateStep();
            }
        }
    }
}
