﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public class Car : MonoBehaviour {

    public static Car instance;

    [Header("Info")]
    public int myLevel;
    public float distanceTarget;

    public bool isStartGame;
    public bool isComplete;
    public bool isBreak;
    public bool isDrift;
    public bool isDirection;

    [Header("Physic")]
    private Rigidbody rig;
    public float moveSpeed;
    public float speedAfterDrift;
    private float smooth;
    private float dir;
    private Vector3 directionDrift;
    private float timeFakeVelocity;
    private float valueDrift;
    public float maxValueDrift;

    [Header("GameObject")]
    public GameObject directionDriftObject;
    public GameObject cameraMain;
    public GameObject taptoplayObject;



    private void Awake()
    {
        instance = this;
        rig = GetComponent<Rigidbody>();
    }

    private void Start () {
        InitGame();
      //  StartGame();

    }

    private void Update () {


        if (isStartGame == false || isComplete)
            return;

  
        RiftControl();
        CarController();


    }


    private void LateUpdate()
    {
        CameraMainController();
    }

    private void InitGame()
    {
        myLevel = PlayerPrefs.GetInt("mylevel");

        distanceTarget = LevelManager.instance.levels[myLevel].distance;

        transform.position = LevelManager.instance.levels[myLevel].startPosition.transform.position;

        Vector3 temp = transform.eulerAngles;
        temp.y = LevelManager.instance.levels[myLevel].angle;
        transform.eulerAngles = temp;

        Vector3 targetPositon = new Vector3(transform.position.x, cameraMain.transform.position.y, transform.position.z);
        cameraMain.transform.position = targetPositon;

        for(int i = 0; i < LevelManager.instance.levels[myLevel].startPositionEnemy.Count; i++)
        {
            GameObject enemy = EnemyPooler.instance.GetObject();
            Debug.Log(enemy.transform.position);
            Debug.Log(LevelManager.instance.levels[myLevel].startPositionEnemy[i].transform.position);

            Vector3 tempe = enemy.transform.eulerAngles;
            tempe.y = transform.eulerAngles.y;
            enemy.transform.position = tempe;

            NavMeshAgent nav = enemy.GetComponent<NavMeshAgent>();
            nav.enabled = true;

            enemy.SetActive(true);
            enemy.transform.position = LevelManager.instance.levels[myLevel].startPositionEnemy[i].transform.position;

        }

        isStartGame = false;
        isComplete = false;
    }

    public void StartGame()
    {
        taptoplayObject.SetActive(false);
        isStartGame = true;

    }


    private void CarController()
    {
        if (isBreak == false)
        {
            if (smooth < 1)
                smooth += (Time.deltaTime * 1f * speedAfterDrift);
        }

        if (isDirection == false)
        {

            transform.Translate(0, 0, moveSpeed * smooth * Time.deltaTime);

            //Vector3 directionMove = directionDriftObject.transform.position - transform.position;
            //rig.velocity = directionMove.normalized * moveSpeed;

        }
        else
        {
            if (timeFakeVelocity > 0)
            {
                timeFakeVelocity -= (Time.deltaTime * .7f);
            }
            transform.position += moveSpeed * Time.deltaTime * directionDrift.normalized * timeFakeVelocity;

        }

        if (isDrift)
        {
            if (smoothRift < 1)
                smoothRift += Time.deltaTime;
        }
        else
        {
            if (smoothRift > 0)
            {
                if (smoothRift > .2f)
                    smoothRift = .2f;

                smoothRift -= Time.deltaTime;
            }
            else
            {
                smoothRift = 0;
            }
        }



        if (isDrift)
        {
           if(isDoubleHolding)
            {
                maxValueDrift = 182;
            }
            else
            {
                maxValueDrift = 94;
            }

            if (valueDrift < maxValueDrift)
               {
                valueDrift += 4 * smoothRift * Mathf.Abs(dir);

                transform.eulerAngles += new Vector3(0, 4 * smoothRift * dir, 0);
            }
        }
        else
        {
            SmartAutoRotate();
        }


    }



    float smoothRift;


    public bool isHoldingLeft;
    public bool isHoldingRight;
    public bool isDoubleHolding;

    public void RiftControl()
    {
        OnTapDownLeft();
        OnTapDownRight();

        OnTapUpLeft();
        OnTapUpRight();

    }

    public void OnTapDownLeft()
    {
        if (Input.GetKey(KeyCode.A))
        {
           // Debug.Log(" PRESS A ");
            if (isHoldingRight)
            {
                isDoubleHolding = true;
                return;
            }

            isHoldingLeft = true;

            dir = -1;

            if (smooth > 0)
            {
                smooth -= Time.deltaTime;
                isBreak = true;
            }

            isDrift = true;


            if (isDirection == false)
            {
                isDirection = true;
                directionDrift = directionDriftObject.transform.position - transform.position;
                timeFakeVelocity = smooth;
                valueDrift = 0;
            }
        }
    }

    public void OnTapUpLeft()
    {

        if (Input.GetKeyUp(KeyCode.A))
        {
           // Debug.Log(" up A");
            if (isHoldingRight)
                return;

            isHoldingLeft = false;
            isHoldingRight = false;


            isBreak = false;
            isDrift = false;
            isDirection = false;

            isDoubleHolding = false;
        }
    }

    public void OnTapDownRight()
    {
        if (Input.GetKey(KeyCode.D))
        {
            //Debug.Log(" PRESS D ");

            if (isHoldingLeft)
            {
                isDoubleHolding = true;
                return;
            }
            isHoldingRight = true;
            dir = 1;

            if (smooth > 0)
            {
                smooth -= Time.deltaTime;
                isBreak = true;
            }

            isDrift = true;


            if (isDirection == false)
            {
                isDirection = true;
                directionDrift = directionDriftObject.transform.position - transform.position;
                timeFakeVelocity = smooth;
                valueDrift = 0;
            }

        }
    }

    public void OnTapUpRight()
    {


        if (Input.GetKeyUp(KeyCode.D))
        {
            //Debug.Log(" up D");
            if (isHoldingLeft)
                return;

            isHoldingLeft = false;
            isHoldingRight = false;


            isBreak = false;
            isDrift = false;
            isDirection = false;

            isDoubleHolding = false;
        }
    }

    private void CameraMainController()
    {
        Vector3 targetPositon = new Vector3(transform.position.x, cameraMain.transform.position.y, transform.position.z);
        cameraMain.transform.position = Vector3.Lerp(cameraMain.transform.position, targetPositon, 3 * Time.deltaTime);

        //Vector3 targetAngle = new Vector3(cameraMain.transform.eulerAngles.x, transform.eulerAngles.y, cameraMain.transform.eulerAngles.z);
        //cameraMain.transform.eulerAngles = Vector3.Lerp(cameraMain.transform.eulerAngles, targetAngle, 3 * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Wall")
        {

            Debug.Log(" collision wall ");
        }

        if(collision.gameObject.tag == "Enemy")
        {
            Debug.Log(" collision enemy ");

            gameObject.SetActive(false);
           collision.gameObject.SetActive(false);

            GameObject explotion = ExplotionPooler.instance.GetObject();
            explotion.transform.position = gameObject.transform.position;
            explotion.SetActive(true);

            GameObject explotion2 = ExplotionPooler.instance.GetObject();
            explotion2.transform.position = collision.gameObject.transform.position;
            explotion2.SetActive(true);

            Complete();
        }
    }

    private void Complete()
    {
        Debug.Log(" complete ");
        isComplete = true;
    }





    public void LoadScene()
    {
        SceneManager.LoadScene(0);
    }


    private const float speedSmartRotate = 40.0f;
    private void SmartAutoRotate()
    {

        if (transform.eulerAngles.y <= 45 || transform.eulerAngles.y > 315)
        {
            Vector3 temp = transform.eulerAngles;

            if (transform.eulerAngles.y <= 45)
            {
                if (temp.y > 2)
                {
                    temp.y -= Time.deltaTime * speedSmartRotate;
                }
                else
                {
                    temp.y = 0;
                }
            }
            else
            {
                if (temp.y < 358)
                {
                    temp.y += Time.deltaTime * speedSmartRotate;
                }
                else
                {
                    temp.y = 0;
                }
            }

            transform.eulerAngles = temp;

        }
        else if (transform.eulerAngles.y <= 315 && transform.eulerAngles.y > 225)
        {
            Vector3 temp = transform.eulerAngles;

            if (temp.y > 272)
            {
                temp.y -= Time.deltaTime * speedSmartRotate;
            }
            else if (temp.y < 268)
            {
                temp.y += Time.deltaTime * speedSmartRotate;
            }
            else
            {
                temp.y = 270;
            }

            transform.eulerAngles = temp;
        }
        else if (transform.eulerAngles.y <= 225 && transform.eulerAngles.y > 135)
        {
            Vector3 temp = transform.eulerAngles;

            if (temp.y > 182)
            {
                temp.y -= Time.deltaTime * speedSmartRotate;
            }
            else if (temp.y < 178)
            {
                temp.y += Time.deltaTime * speedSmartRotate;
            }
            else
            {
                temp.y = 180;
            }

            transform.eulerAngles = temp;
        }
        else if (transform.eulerAngles.y <= 135 && transform.eulerAngles.y > 45)
        {
            Vector3 temp = transform.eulerAngles;

            if (temp.y > 92)
            {
                temp.y -= Time.deltaTime * speedSmartRotate;
            }
            else if (temp.y < 88)
            {
                temp.y += Time.deltaTime * speedSmartRotate;
            }
            else
            {
                temp.y = 89;
            }

            transform.eulerAngles = temp;
        }
    }
}
